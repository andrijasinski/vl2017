### **Tiimi liikmed:** ###

Andri Jasinski

Elisaveta Saveljev

Joel Jakob Koel

### **Testkeskond:** ###
Vana: [www.katiga.ee/vl2017/](http://www.katiga.ee/vl2017/)

Uus: [http://ekool.artur.cs.ut.ee/](http://ekool.artur.cs.ut.ee/)

[phpMyAdmin](https://veebimajutus.elion.ee/external/phpMyAdmin3/)


### **Etapid:** ###

[1. etapp ](https://bitbucket.org/andrijasinski/vl2017/wiki/1%20etapp)

[2. etapp ](https://bitbucket.org/andrijasinski/vl2017/wiki/2%20Etapp)

[3. etapp ](https://bitbucket.org/andrijasinski/vl2017/wiki/3.etapp)

[4. etapp ](https://bitbucket.org/andrijasinski/vl2017/wiki/4%20Etapp)

[5. etapp ](https://bitbucket.org/andrijasinski/vl2017/wiki/5%20Etapp)

[6. etapp ](https://bitbucket.org/andrijasinski/vl2017/wiki/6%20Etapp)