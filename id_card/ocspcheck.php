<?php
// User certificate issuer certificate file location
$ocsp_info = Array();

// EID-SK 2011
$ocsp_info["EID-SK 2011"]["CA_CERT_FILE"]="certs/live/EID-SK_2011.crt";
$ocsp_info["EID-SK 2011"]["OCSP_SERVER_URL"]='http://ocsp.sk.ee';
$ocsp_info["EID-SK 2011"]["OCSP_SERVER_CERT_FILE"]="certs/live/SK_OCSP_RESPONDER_2011.pem.cer";


// Checking status of certificates issued from "ESTEID-SK 2011" against live OCSP
// ESTEID-SK 2011 - CA for Estonian national ID-card certificates issued since 2011
$ocsp_info["ESTEID-SK 2011"]["CA_CERT_FILE"]="certs/live/ESTEID-SK_2011.crt";
$ocsp_info["ESTEID-SK 2011"]["OCSP_SERVER_URL"]='http://ocsp.sk.ee';
$ocsp_info["ESTEID-SK 2011"]["OCSP_SERVER_CERT_FILE"]="certs/live/SK_OCSP_RESPONDER_2011.pem.cer";


// Checking status of certificates issued from "ESTEID-SK 2015" against live OCSP
// ESTEID-SK 2015
$ocsp_info["ESTEID-SK 2015"]["CA_CERT_FILE"]="certs/live/ESTEID-SK_2015.pem.crt";
$ocsp_info["ESTEID-SK 2015"]["OCSP_SERVER_URL"]='http://ocsp.sk.ee';
$ocsp_info["ESTEID-SK 2015"]["OCSP_SERVER_CERT_FILE"]="certs/live/SK_OCSP_RESPONDER_2011.pem.cer";


// KLASS3-SK 2010 - CA for company certificates KLASS3-SK 2010 (EECCRCA, SHA384)
$ocsp_info["KLASS3-SK 2010"]["CA_CERT_FILE"]="certs/live/KLASS3-SK_2010_EECCRCA_SHA384.pem.crt";
$ocsp_info["KLASS3-SK 2010"]["OCSP_SERVER_URL"]='http://ocsp.sk.ee';
$ocsp_info["KLASS3-SK 2010"]["OCSP_SERVER_CERT_FILE"]="certs/live/SK_OCSP_RESPONDER_2011.pem.cer";


// TEST of ESTEID-SK 2011 - CA for test certificates
$ocsp_info["TEST of ESTEID-SK 2011"]["CA_CERT_FILE"]="certs/test/test_esteid_2011.crt";
$ocsp_info["TEST of ESTEID-SK 2011"]["OCSP_SERVER_URL"]='http://demo.sk.ee/ocsp';
$ocsp_info["TEST of ESTEID-SK 2011"]["OCSP_SERVER_CERT_FILE"]="certs/test/TEST_of_SK_OCSP_RESPONDER_2011.pem.cer";


// TEST of ESTEID-SK 2015 - CA for test certificates
$ocsp_info["TEST of ESTEID-SK 2015"]["CA_CERT_FILE"]="certs/test/TEST_of_ESTEID-SK_2015.pem.crt";
$ocsp_info["TEST of ESTEID-SK 2015"]["OCSP_SERVER_URL"]='http://demo.sk.ee/ocsp';
$ocsp_info["TEST of ESTEID-SK 2015"]["OCSP_SERVER_CERT_FILE"]="certs/test/TEST_of_SK_OCSP_RESPONDER_2011.pem.cer";


// TEST of EID-SK 2011
$ocsp_info["TEST of EID-SK 2011"]["CA_CERT_FILE"]="certs/test/TEST_of_EID-SK_2011.pem.crt";
$ocsp_info["TEST of EID-SK 2011"]["OCSP_SERVER_URL"]='http://demo.sk.ee/ocsp';
$ocsp_info["TEST of EID-SK 2011"]["OCSP_SERVER_CERT_FILE"]="certs/test/TEST_of_SK_OCSP_RESPONDER_2011.pem.cer";


// TEST of EID-SK 2016
$ocsp_info["TEST of EID-SK 2016"]["CA_CERT_FILE"]="certs/test/TEST_of_EID-SK_2016.pem.crt";
$ocsp_info["TEST of EID-SK 2016"]["OCSP_SERVER_URL"]='http://demo.sk.ee/ocsp';
$ocsp_info["TEST of EID-SK 2016"]["OCSP_SERVER_CERT_FILE"]="certs/test/TEST_of_SK_OCSP_RESPONDER_2011.pem.cer";

// TEST of NQ-SK 2016
$ocsp_info["TEST of NQ-SK 2016"]["CA_CERT_FILE"]="certs/test/TEST_of_NQ-SK_2016.pem.crt";
$ocsp_info["TEST of NQ-SK 2016"]["OCSP_SERVER_URL"]='http://demo.sk.ee/ocsp';
$ocsp_info["TEST of NQ-SK 2016"]["OCSP_SERVER_CERT_FILE"]="certs/test/TEST_of_SK_OCSP_RESPONDER_2011.pem.cer";


// TEST of KLASS3-SK 2016
$ocsp_info["TEST of KLASS3-SK 2016"]["CA_CERT_FILE"]="certs/test/TEST_of_KLASS3-SK_2016.pem.crt";
$ocsp_info["TEST of KLASS3-SK 2016"]["OCSP_SERVER_URL"]='http://demo.sk.ee/ocsp';
$ocsp_info["TEST of KLASS3-SK 2016"]["OCSP_SERVER_CERT_FILE"]="certs/test/TEST_of_SK_OCSP_RESPONDER_2011.pem.cer";

// Openssl binary location
$ocsp_info["OPEN_SSL_BIN"] = '/usr/bin/openssl';

// Temp folder to store certificates
$ocsp_info["OCSP_TEMP_DIR"] = '/var/tmp/';

// When true, then OCSP check will be made
$ocsp_info["OCSP_ENABLED"] = true;


/*
Params:
$cert - user certificate in PEM format

Output:
 0 - OCSP certificate status unknown
 1 - OCSP certificate status good / valid
 2 - OCSP internal error
 3 - OCSP certificate status revoked
 4 - Some error in script
*/

function doOCSPcheck($cert) {

	global $ocsp_info; // Global config array
	
	$user_good = 0;
	$issuer_dn=$_SERVER["SSL_CLIENT_I_DN_CN"];
	
	if ($ocsp_info["OCSP_ENABLED"]===false) {
		return Array("OCSP_ENABLED === false", 0);
	}

	// Saving user certificate file to OCSP temp folder
	$tmp_f = fopen($tmp_f_name = tempnam($ocsp_info["OCSP_TEMP_DIR"],'ocsp_check'),'w');
	fwrite($tmp_f,$cert);
	fclose($tmp_f);

	if ($ocsp_info["OCSP_ENABLED"] && isset($ocsp_info[$issuer_dn]["CA_CERT_FILE"]) && isset($ocsp_info[$issuer_dn]["OCSP_SERVER_CERT_FILE"]) && isset($ocsp_info[$issuer_dn]["OCSP_SERVER_URL"])) {


		// Making OCSP request using OpenSSL ocsp command
		$command = $ocsp_info["OPEN_SSL_BIN"].' ocsp -issuer '.$ocsp_info[$issuer_dn]["CA_CERT_FILE"].' -cert '.$tmp_f_name.' -url '.$ocsp_info[$issuer_dn]["OCSP_SERVER_URL"].' -VAfile '.$ocsp_info[$issuer_dn]["OCSP_SERVER_CERT_FILE"];

		$descriptorspec = array(
		   0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
		   1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
		   2 => array("pipe", "w") // stderr is a pipe that the child will write to
		);

		$process = proc_open($command, $descriptorspec, $pipes);

		if (is_resource($process)) {
			fclose($pipes[0]);


			// Getting errors from stderr
			$errorstr="";
			while ($line = fgets($pipes[2])) {
				$errorstr.=$line;
			}

			if ($errorstr!="" && (strpos($errorstr,"Response verify OK")!==0)) {
				$user_good = 4;
			} else {
				// Parsing OpenSSL command stdout
				while ($line = fgets($pipes[1])) {
					if (strstr($line,'good')) {
						$user_good = 1;
					} else if (strstr($line,'internalerror (2)')) {
						$user_good = 2;
					} else if (strstr($line,'revoked')) {
						$user_good = 3;
					}
				}
				fclose($pipes[1]);
			}

			proc_close($process);
		}
	}

	return Array($errorstr, $user_good);
}
?>