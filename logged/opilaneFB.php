<?php

// Include FB config file && User class
require_once 'fb/fbConfig.php';
require_once 'fb/User.php';

if(isset($accessToken)){
    if(isset($_SESSION['facebook_access_token'])){
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }else{
        // Put short-lived access token in session
        $_SESSION['facebook_access_token'] = (string) $accessToken;

        // OAuth 2.0 client handler helps to manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Exchanges a short-lived access token for a long-lived one
        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

        // Set default access token to be used in script
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }

// Redirect the user back to the same page if url has "code" parameter in query string
    if(isset($_GET['code'])){
        header('Location:');
    }

// Getting user facebook profile info
    try {
        $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,picture');
        $fbUserProfile = $profileRequest->getGraphNode()->asArray();
    } catch(FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        session_destroy();
        // Redirect user back to app login page
        header("Location: ./");
        exit;
    } catch(FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

// Initialize User class
    $user = new User();

// Insert or update user data to the database
    $fbUserData = array(
        'oauth_provider'=> 'facebook',
        'oauth_uid'     => $fbUserProfile['id'],
        'first_name'    => $fbUserProfile['first_name'],
        'last_name'     => $fbUserProfile['last_name'],
        'email'         => $fbUserProfile['email'],
        'gender'        => $fbUserProfile['gender'],
        'locale'        => $fbUserProfile['locale'],
        'picture'       => $fbUserProfile['picture']['url'],
        'link'          => $fbUserProfile['link']
    );
    $userData = $user->checkUser($fbUserData);

// Put user data into session
    $_SESSION['userData'] = $userData;

// Get logout url
    $logoutURL = $helper->getLogoutUrl($accessToken, '/fb/logout.php');

// Render facebook profile data
    if(!empty($userData)){
        $output  = '<h1>Facebook Profile Details </h1>';
        $output .= '<img src="'.$userData['picture'].'">';
        $output .= '<br/>Facebook ID : ' . $userData['oauth_uid'];
        $output .= '<br/>Name : ' . $userData['first_name'].' '.$userData['last_name'];
        $output .= '<br/>Email : ' . $userData['email'];
        $output .= '<br/>Gender : ' . $userData['gender'];
        $output .= '<br/>Locale : ' . $userData['locale'];
        $output .= '<br/>Logged in with : Facebook';
        $output .= '<br/><a href="'.$userData['link'].'" target="_blank">Click to Visit Facebook Page</a>';
        $output .= '<br/>Logout from <a href="'.$logoutURL.'">Facebook</a>';
    }else{
        $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="est" xml:lang="est" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Veebirakenduste loomine 2017"/>
    <meta name="author" content="Joel Jakob Koel"/>
    <title>eKool 2.0</title>
    <link rel="stylesheet" type="text/css" href="../Style.css"/>
</head>
<body class="opilane">
<div id="logopilt"><a class="logo1" href="logout.php"><img class="logo1" src="../images/STEM_Logo.jpg" alt="Logo" /></a></div>
<div id="logout"><p><input id="logivälja" type="button" value="Logi välja" onclick="window.location.href=<?php echo $logoutURL ?>"/></p>
<br/>Logout from <a href="<?php echo $logoutURL?>">Facebook</a></div>

<hr/>
<div style="margin-left: 10%;" >
    <img src="<?php echo $userData['picture'] ?>" style="vertical-align: middle;" alt="Portree"/>
    <span style="vertical-align: middle;"><?php echo $userData['first_name'].' '.$userData['last_name'] ?></span>
</div>
<br/>

<div class="tab">
    <button class="tablinks" onclick="tab(event, 'Uudised')">Uudised
        <span class="tooltip">Viimati lisatud hinded ja ülesanded</span>
    </button>
    <button class="tablinks" onclick="tab(event, 'Hinded')">Hinded
        <span class="tooltip">Viimased hinded</span>
    </button>

    <button class="tablinks" onclick="tab(event, 'Ülesanded')">Ülesanded
        <span class="tooltip">Kodused ülesanded</span>
    </button>
</div>

<div id="Uudised" class="tabcontent">
    <table style="width:100%">
        <col width="10%"/>
        <col width="20%"/>
        <tr>
            <td class="kesk">5</td>
            <td>Bioloogia</td>
            <td>Tunnihinne: Kahepaiksed</td>
        </tr>
        <tr>
            <td class="kesk">4</td>
            <td>Bioloogia</td>
            <td>Kontrolltöö hinne: Linnud</td>
        </tr>
        <tr>
            <td class="kesk">Kodutöö</td>
            <td>Matemaatika</td>
            <td>TV lk 12-15</td>
        </tr>
    </table>
</div>

<div id="Hinded" class="tabcontent">
    <table style="width:100%">
        <col width="20%"/>
        <tr>
            <th align="left">Aine nimetus</th>
            <th>Hinded</th>
        </tr>
        <tr>
            <td>Füüsika</td>
            <td>5, 4, 5, 4, 5, 5</td>
        </tr>
        <tr>
            <td>Keemia</td>
            <td>5, 5, 4</td>
        </tr>
        <tr>
            <td>Ajalugu</td>
            <td>4, 3, 4</td>
        </tr>
    </table>
</div>

<div id="Ülesanded" class="tabcontent">
    <table style="width:100%">
        <col width="10%"/>
        <col width="20%"/>
        <tr>
            <td>02.03</td>
            <td>Matemaatika</td>
            <td>Ül 546-549</td>
        </tr>
        <tr>
            <td>02.03</td>
            <td>Füüsika</td>
            <td>Õp lk 54-61</td>
        </tr>
        <tr>
            <td>02.03</td>
            <td>Ajalugu</td>
            <td>Õp lk 78-86</td>
        </tr>
    </table>
</div>

<script type="text/javascript" src="../script.js"></script>
</body>
</html>