<?php
session_start();
if (!isset($_SESSION['logged_in'])){
	header("location:../index.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="est" xml:lang="est" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Veebirakenduste loomine 2017"/>
	<meta name="author" content="Joel Jakob Koel"/>
	<title>eKool 2.0</title>
	<link rel="stylesheet" type="text/css" href="../Style.css"/>
	</head>
<body class="opetaja">
	<div id="logopilt"><a class="logo1" href="logout.php"><img class="logo1" src="../images/STEM_Logo.jpg" alt="Logo" /></a></div>
	<div id="logout"><p><input id="logivälja" type="button" value="Logi välja" onclick="window.location.href='logout.php'"/></p></div> 

	<hr/>
	<div style="margin-left: 10%;">
		<img src="../images/default_portrait.jpg" style="vertical-align: middle" alt="Portree"/>
		<span style="vertical-align: middle;">Kasutaja nimi</span>
		<br/>
		<table style="width:103%;">
		<tr>
			<td>Test klass</td>
			<td>
				<select style="margin-left: 106%; display: inline-block" onchange="return getval(this)">
				  <option value="paevik">Päevik</option>
				  <option value="kodutoo">Kodutööd</option>
				</select>
			</td>
		</tr>
		</table>
	</div>
	<br/>

	<div style="margin-left: 10%;">
		<table style="width:114%;">
			<col width="25%"/>
			<col width="75%"/>
			<tr>
				<td>Õpilase nimi</td>
				<td>Hinded</td>
				<td><button id="lisa">+</button></td>
			</tr>
			<tr>
				<td>Õpilane 1</td>
				<td>5 3 2</td>
			</tr>
			<tr>
				<td>Õpilane 2</td>
				<td>5 5 5-</td>
			</tr>
		</table>
	</div>

	<div id="mypopup" class="popup">
	  <div class="popup-content">
		<span class="close">&times;</span>
		<form action="" method="post">  
			<label for="kuupäev">Kuupäev: </label>
			<input id="kuupäev" type="date" name="kuupäev"/><br/><br/>
			<label for="tunniteema">Tunniteema: </label>
			<input id="tunniteema" type="text" name="tunniteema" size="35"/><br/><br/>
			<label for="kirjeldus">Kirjeldus: </label>
			<input id="kirjeldus" type="text" name="kirjeldus" size="100%"/><br/><br/>
			<input type="submit" value="Salvesta"/>  
		</form>
	  </div>
	</div>

	<script type="text/javascript">
		var popup = document.getElementById('mypopup');
		var btn = document.getElementById("lisa");
		var span = document.getElementsByClassName("close")[0];

		btn.onclick = function() {
			popup.style.display = "block";
		}

		span.onclick = function() {
			popup.style.display = "none";
		}

		window.onclick = function(event) {
			if (event.target == popup) {
				popup.style.display = "none";
			}
		}
	</script>
</body>
</html>