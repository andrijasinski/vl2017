<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="est" xml:lang="est" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Veebirakenduste loomine 2017"/>
	<title>eKool 2.0</title>
	<script src=" https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="app.js"></script>
	<link rel="stylesheet" type="text/css" href="../Style.css"/>
	</head>
<body>
<a href="nimed.html"> Team members </a>
<div id="nimed"></div>

<div class="coderepo" align="right"><h1>Code repo:</h1>
  <p><a href="https://bitbucket.org/andrijasinski/vl2017/overview">https://bitbucket.org/andrijasinski/vl2017/overview</a></div>
<h1>Where we are:</h1>
<p><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1965.9609368988447!2d26.712883159683916!3d58.37812655950618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcfe47bde7213b445!2sInstitute+of+Computer+Science+of+University+of+Tartu!5e0!3m2!1sru!2sru!4v1491552312719" width="600" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
</body>
</html>