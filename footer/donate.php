<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Example payment usage - Swedbank - Pangalink-net</title>
		<link rel="stylesheet" type="text/css" href="../Style.css"/>
    </head>
    <body>
<?php

// THIS IS AUTO GENERATED SCRIPT
// (c) 2011-2017 Kreata OÜ www.pangalink.net

// File encoding: UTF-8
// Check that your editor is set to use UTF-8 before using any non-ascii characters

// STEP 1. Setup private key
// =========================

$private_key = openssl_pkey_get_private(
"-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAuCr3LlKiMTOHbtthz7ZXQdxr/pEBWijRUzfBgQJmXdyqrJF0
nxc0C5E2nrIyDKiaK5hY5Mr4/29L5xpwItHjbiuT8/7BPVYxIllCKEga+e4x8xru
Do7EgCgRED4LDwdlUTbAKL9rAgBdvmxEjDkltN64dzVrLehDLyZnbMrowNckOvf/
LK5iKxUp4ruWgw0rCvGFHCkRSKT1GFZC5dAUbcOebkN1b8Wt63jK6UihmH7wLj3F
+ogY1rpzOlnIg2gDr6mWpyOhwzgoQ77kSjXJNo6a8x30VK0XebnHvoIJ10zlHEPJ
RxH/QYOC8pXPpSScL6P3mXUmy2BSU/d2OooANwIDAQABAoIBAQCSOyNbN6erE3Q0
9KK8b9bm56MkVXTSZtiWkA3oZsRrQNyP+0qzS5Q6nwnUhj8xR+pqNSRYPkXpZDDL
hnWFYEyKXLW0phI/pqGK2X6rR12MF7K3iil9i6TA8Y9i6zCStVegXa3EbQEFbM8C
dSsUgAfNjo6/sL2yjNpTpNW4T4DyJEivSK4kozDX6DPAUveZM2qyjbjiqBTf49AO
VTwAVufj4J6v+YhtezZ+EZWdo0EBQv+JzQt8mn7xz7SRhqO2x0pNDCRgVrjJukf3
YS4hJ+wuX572YO155bvQwQVuJi/JSA7j5f8DkrR9X0H1eKgIQsThqtkVNZHlE6gm
J/lR7x+5AoGBAN2mLp8fIRHQbBUgM/rEHFVvN8PQE6rH+L8oUc15OYuW+mMYZ5HG
J9/WXF/VmtZouBC9afQUQyiVxOYjvoZh0TJiG4dD4FLcchh65zap/PTyMThbw2VS
BQtPQUL/6EZOP9IZsoxHwoPWd6iyp1vaVJY5D6m5R0KCLiE8Q8P6OEoTAoGBANS1
uzHkF9UADS5v/tClLolGu6HW7dCbso1jIlLkDjO8f42tJBeKF/LJOeXeT9xun9oK
PxhEE2uWBTfV+Njf8746ZBgSxzYVvFFGjn88zpMSRQvb7aKPx0N+dWYLqGjiizni
fgSWLjXwR44NPzgtXhDGN16lcPR0EUTTb/JLdHXNAoGAYDdAa8CWmWYRVQZaqhOx
fQC/g1fzAaK2/zwgboKeEVNjs9qnMueC7a7fAJb4FPT2WgXvVY3qLVb+VyzTCwCx
UB64dnpniXC6cpf/gIJAFZCA/0cORhBZsjQgT1ZJXSOgnrKbCDwqWxKhdPDAp6Xa
LC/u9a1BGsTqA3FmtU8MWlMCgYApdBE7M9yBIjVva1wZhVeAYUyHyb8m/HDbpQo3
ZUJvSrkA/1EdVUg9hIHIDn850kVM1Npe/EVcniv+KbEJcvupgu+m0WIJZEsC++Eh
YtRPTSwvl3jESYjn2mk+rd7wMdY4Pa+ZbTssP9tyD5rWc+D1G+wGge91dHk5FIuo
iJP1sQKBgAOhqpRZgQvhnEsyZFuuI3OmjXhTK2TPDZevo3mDMhhx8tZHIPzyonP9
U1540Y38CyjQZZTESRLatFa1kQsH+Q07kVCLFEFFkOVjj17En2J5OwEXEy3qCvB3
3VxCFeq64rgnt3aAya0Knzu0H1Yi6CgRGae+A3+Gq2dT8G8Ik+/p
-----END RSA PRIVATE KEY-----");

// STEP 2. Define payment information
// ==================================

$fields = array(
        "VK_SERVICE"     => "1011",
        "VK_VERSION"     => "008",
        "VK_SND_ID"      => "uid100010",
        "VK_STAMP"       => "12345",
        "VK_AMOUNT"      => "150",
        "VK_CURR"        => "EUR",
        "VK_ACC"         => "EE512200221030299217",
        "VK_NAME"        => "Andri Jasinski",
        "VK_REF"         => "1234561",
        "VK_LANG"        => "EST",
        "VK_MSG"         => "Torso Tiger",
        "VK_RETURN"      => "http://localhost:8080/project/Rz8kVmp3r13Fpo19?payment_action=success",
        "VK_CANCEL"      => "http://localhost:8080/project/Rz8kVmp3r13Fpo19?payment_action=cancel",
        "VK_DATETIME"    => "2017-04-09T18:41:43+0300",
        "VK_ENCODING"    => "utf-8",
);

// STEP 3. Generate data to be signed
// ==================================

// Data to be signed is in the form of XXXYYYYY where XXX is 3 char
// zero padded length of the value and YYY the value itself
// NB! Swedbank expects symbol count, not byte count with UTF-8,
// so use `mb_strlen` instead of `strlen` to detect the length of a string

$data = str_pad (mb_strlen($fields["VK_SERVICE"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_SERVICE"] .    /* 1011 */
        str_pad (mb_strlen($fields["VK_VERSION"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_VERSION"] .    /* 008 */
        str_pad (mb_strlen($fields["VK_SND_ID"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_SND_ID"] .     /* uid100010 */
        str_pad (mb_strlen($fields["VK_STAMP"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_STAMP"] .      /* 12345 */
        str_pad (mb_strlen($fields["VK_AMOUNT"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_AMOUNT"] .     /* 150 */
        str_pad (mb_strlen($fields["VK_CURR"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_CURR"] .       /* EUR */
        str_pad (mb_strlen($fields["VK_ACC"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_ACC"] .        /* EE512200221030299217 */
        str_pad (mb_strlen($fields["VK_NAME"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_NAME"] .       /* Andri Jasinski */
        str_pad (mb_strlen($fields["VK_REF"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_REF"] .        /* 1234561 */
        str_pad (mb_strlen($fields["VK_MSG"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_MSG"] .        /* Torso Tiger */
        str_pad (mb_strlen($fields["VK_RETURN"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_RETURN"] .     /* http://localhost:8080/project/Rz8kVmp3r13Fpo19?payment_action=success */
        str_pad (mb_strlen($fields["VK_CANCEL"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_CANCEL"] .     /* http://localhost:8080/project/Rz8kVmp3r13Fpo19?payment_action=cancel */
        str_pad (mb_strlen($fields["VK_DATETIME"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_DATETIME"];    /* 2017-04-09T18:41:43+0300 */

/* $data = "0041011003008009uid10001000512345003150003EUR020EE512200221030299217014Andri Jasinski0071234561011Torso Tiger069http://localhost:8080/project/Rz8kVmp3r13Fpo19?payment_action=success068http://localhost:8080/project/Rz8kVmp3r13Fpo19?payment_action=cancel0242017-04-09T18:41:43+0300"; */

// STEP 4. Sign the data with RSA-SHA1 to generate MAC code
// ========================================================

openssl_sign ($data, $signature, $private_key, OPENSSL_ALGO_SHA1);

/* YEakX/QN2JdCbVLT2zMAUPhetnlZgaz4QQkVlARMy4RrfZelAi79Ui1uNbK4Uc0kj+57seqfj9N3DLixWyljX5+jPbuLnAsmbugZZzGmVGQ+Ct4f1DqkmkvjkPOsU0IU5LmwjGHs1REYf58OPBn1XcyAk+CDWbBCDD2jqTIJQ+ECJ/ey1CB1QMDgMEneXO9gN5WEEE37+WQ8hrI3iK2Rt3FUnSDcg0FhE71/zgb5EWljUpvjXHyMEuuQCaJZp1AU7q9ufn/VvO4HuZIKL/bTIXlzpF5rT8gT6LeVboTw5SuBL6l69IhZzy3Iuh7oV+CRR75N9I2/H/Ri/YOqoVZrgg== */
$fields["VK_MAC"] = base64_encode($signature);

// STEP 5. Generate POST form with payment data that will be sent to the bank
// ==========================================================================
?>

        <h1>Donate for new eKool</a></h1>
        <p>Tere! Täname Teid, et soovite aidata meile projekti arendamises</p>

        <form method="post" action="http://localhost:8080/banklink/swedbank-common">
            <!-- include all values as hidden form fields -->
<?php foreach($fields as $key => $val):?>
            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo htmlspecialchars($val); ?>" />
<?php endforeach; ?>
		
                <!-- when the user clicks "Edasi panga lehele" form data is sent to the bank -->
                <tr><td colspan="2"><input type="submit" value="Edasi panga lehele" /></td></tr>
            </table>
        </form>

    </body>
</html>
