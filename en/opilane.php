<?php
session_start();
if (!isset($_SESSION['logged_in'])){
	header("location:/index.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="est" xml:lang="est" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Veebirakenduste loomine 2017"/>
	<meta name="author" content="Joel Jakob Koel"/>
	<title>eKool 2.0</title>
	<link rel="stylesheet" type="text/css" href="../Style.css"/>
	</head>
<body class="opilane">
<div id="logopilt"><a class="logo1" href="logout.php"><img class="logo1" src="../images/STEM_Logo.jpg" alt="Logo" /></a></div>
<div id="logout"><p><input id="logivälja" type="button" value="Log out" onclick="window.location.href='logout.php'"/></p></div> 

<hr/>
<div style="margin-left: 10%;" >
	<img src="../images/default_portrait.jpg" style="vertical-align: middle;" alt="Portree"/>
	<span style="vertical-align: middle;">User Name</span>
</div>
<br/>

<div class="tab">
  <button class="tablinks" onclick="tab(event, 'Uudised')">News
  <span class="tooltip">Lately added grades and homeworks</span>
</button>
  <button class="tablinks" onclick="tab(event, 'Hinded')">Grades
    <span class="tooltip">Lately added grades</span>
</button>
  
  <button class="tablinks" onclick="tab(event, 'Ülesanded')">Homeworks
    <span class="tooltip">Homeworks to do</span>
</button>
</div>

<div id="Uudised" class="tabcontent">
	<table style="width:100%">
	<col width="10%"/>
	<col width="20%"/>
	<tr>
		<td class="kesk">5</td>
		<td>Bioloogia</td>
		<td>Tunnihinne: Kahepaiksed</td>
	</tr>
	<tr>
		<td class="kesk">4</td>
		<td>Bioloogia</td>
		<td>Kontrolltöö hinne: Linnud</td>
	</tr>
	<tr>
		<td class="kesk">Kodutöö</td>
		<td>Matemaatika</td>
		<td>TV lk 12-15</td>
	</tr>
	</table>
</div>

<div id="Hinded" class="tabcontent">
	<table style="width:100%">
	  <col width="20%"/>
	  <tr>
		<th align="left">Course name</th>
		<th>Grades</th>
	  </tr>
	  <tr>
		<td>Füüsika</td>
		<td>5, 4, 5, 4, 5, 5</td>
	  </tr>
	  <tr>
		<td>Keemia</td>
		<td>5, 5, 4</td>
	  </tr>
	  <tr>
		<td>Ajalugu</td>
		<td>4, 3, 4</td>
	  </tr>
	</table>
</div>

<div id="Ülesanded" class="tabcontent">
	<table style="width:100%">
	  <col width="10%"/>
	  <col width="20%"/>
	  <tr>
		<td>02.03</td>
		<td>Matemaatika</td>
		<td>Ül 546-549</td>
	  </tr>
	  <tr>
		<td>02.03</td>
		<td>Füüsika</td>
		<td>Õp lk 54-61</td>
	  </tr>
	  <tr>
		<td>02.03</td>
		<td>Ajalugu</td>
		<td>Õp lk 78-86</td>
	  </tr>
	</table>
</div>

<script type="text/javascript" src="script.js"></script>
</body>
</html>