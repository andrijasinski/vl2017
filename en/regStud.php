<?php
$db_hostname = "localhost:3306";
$db_username = "andrijasinski";
$db_password = "lUwWohGb348JyOOo2Q";
$db_database = "andrijasinski_eKool";

// Database Connection String
$con = mysql_connect($db_hostname,$db_username,$db_password);
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }

mysql_select_db($db_database, $con);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="est" xml:lang="est" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Veebirakenduste loomine 2017"/>
	<meta name="author" content="Andri Jasinski"/>
	<title>Sign up new student</title>
	<link rel="stylesheet" type="text/css" href="../Style.css"/>
	</head>
<body>
	<form method="post" action="../register/handlerstud.php">
		<fieldset>
			<legend>Student:</legend>
			<label for="email">Email:</label>
			<input id ="email" type="text" name="email"/><br/><br/>
			<label for="studPass">Password:</label>
			<input id ="studPass" type="password" name="studPass"/><br/><br/>
			<label for="studName">First name:</label>
			<input id ="studName" type="text" name="studName"/><br/><br/>
			<label for="studSecName">Second name:</label>
			<input id ="studSecName" type="text" name="studSecName"/><br/><br/>
			<label for="isikk">National ID:</label>
			<input id ="isikk" type="text" name="isikk"/><br/><br/>
			<label for="linnad">City:</label>
				<select id="linnad" name="linn">
					<?php   
					$query="SELECT linn FROM linnad";
					$result=mysql_query($query) or die ("Query to get data failed: ".mysql_error());
					
					while ($row=mysql_fetch_array($result)) {
					$linn=$row["linn"];
						echo "<option>
							$linn
						</option>";
					}                
					?>
				</select>
			<br/><br/>
			<label for="koolid">School:</label>
				<select id="koolid" name="kool">
					<?php   
					$query="SELECT kool FROM koolid";
					$result=mysql_query($query) or die ("Query to get data failed: ".mysql_error());            
					while ($row=mysql_fetch_array($result)) {
					$kool=$row["kool"];
						echo "<option>
							$kool
						</option>";         
					} 
					?>
				</select>
			<br/><br/>
			<label for="klassid">Room:</label>
				<select id="klassid" name="klass">
					<?php   
					$query="SELECT klass FROM klassid";
					$result=mysql_query($query) or die ("Query to get data failed: ".mysql_error());
					
					while ($row=mysql_fetch_array($result)) {
					$klass=$row["klass"];
						echo "<option>
							$klass
						</option>"; 
					}                
					?>
				</select>
			<br/><br/>
			<input type="submit"/><br/>
		</fieldset>
	</form>
</body>
</html>
