<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="est" xml:lang="est" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Veebirakenduste loomine 2017"/>
	<meta name="author" content="Andri Jasinski"/>
	<title>Sign Up</title>
	<link rel="stylesheet" type="text/css" href="../Style.css"/>
	</head>
<body>
<div class="header">Sign up as:</div>
<input type="button" id="btnOpilane" value="Student" onclick="window.location.href='regstud.php'"/>
<input type="button" id="btnOpitaja" value="Teacher" onclick="window.location.href='regteach.php'"/>
</body>
</html>
