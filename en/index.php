<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="est" xml:lang="est" xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Veebirakenduste loomine 2017"/>
	<meta name="author" content="Joel Jakob Koel"/>
	<title>eKool 2.0</title>
	<link rel="stylesheet" type="text/css" href="../Style.css"/>
	</head>
<body>
<div class="header">eKool 2.0</div>
<input type="button" id="btnsign" value="Sign Up" onclick="window.location.href='registerchoise.php'"/>

<form name="login" action="login.php" method="post">
	<p id="kas"></p>
	<div id="lahter_kas"></div>
	<p id="pw"></p>
	<div id="lahter_pw"></div>
	<input type="button" id="btnlog" value="Log in" onclick="lahtrid();this.setAttribute('onclick','log()')"/>
</form>
<div class="fb-login-button"><?php echo $output?></div>
<a href="../index.php">
<img border="0" alt="Est" src="/../images/est.png" width="50" height="50" align="right">
</a>
<a href="index.php">
<img class="engb" border="0" alt="Eng" src="/../images/gb.png" width="50" height="50" align="right">
</a>
<footer>
<a href="/../footer/meist.php">About us</a>
<a href="/../opilane.html">opilane</a>
<a href="/../footer/donate.php">Donate</a>
<a href="/../footer/stats.php">Statistics</a>

</footer>

<script type="text/javascript" src="script.js"></script>
</body>
</html>
